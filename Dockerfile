FROM maven AS builder
COPY . .
RUN mvn install

FROM openjdk:8-jdk-alpine
COPY --from=builder target/calculadora-service-0.1.0.jar .
ENTRYPOINT ["java", "-jar", "./calculadora-service-0.1.0.jar"]